package com.personal.Actividad.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.personal.Actividad.model.Tareas;

public interface ITareaRepository extends CrudRepository<Tareas, Long>{

}
