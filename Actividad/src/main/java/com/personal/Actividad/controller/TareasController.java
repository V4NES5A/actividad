package com.personal.Actividad.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.Actividad.model.Tareas;
import com.personal.Actividad.model.Tareas.Tipo;
import com.personal.Actividad.repositorio.ITareaRepository;

/**
 * tareasController
 */
@Controller
@RequestMapping("tareas")
@CrossOrigin
public class TareasController {

	@Autowired
	ITareaRepository rTarea;

	// LISTAR
	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Tareas> getAll() {
		return (List<Tareas>) rTarea.findAll();
	}

	// metodo para guardar el registro de la tabla animales//
		@PutMapping(value = "save", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		@CrossOrigin
		public HashMap<String, String> save(@RequestParam Long codigo, @RequestParam String titulo,
				@RequestParam String descripcion, @RequestParam Tipo tipo) {
			
			Tareas an = new Tareas();
			//creando objeto de la clase animales//
			codigo = rTarea.count() + 3;
			
			an = new Tareas(codigo, titulo, descripcion, tipo);

			HashMap<String, String> jsonReturn = new HashMap<>();

			try {
				// guardando el objeto de la clase animales//
				rTarea.save(an);

				// Mensajes deConfirmacion //
				jsonReturn.put("Estado", "OK");
				jsonReturn.put("Mensaje", "Registro guardado");

				// Retornando_Mensaje //
				return jsonReturn;
			} catch (Exception e) {

				// Mensajes_De_Confirmacion //
				jsonReturn.put("Estado", "Error");
				jsonReturn.put("Mensaje", "Registro no guardado");

				// Retornando Mensaje //
				return jsonReturn;
			}
		}

		// metodo para actualizar los registros//
		@PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		@CrossOrigin
		public HashMap<String, String> update(@RequestParam Long codigo, @RequestParam String titulo,
				@RequestParam String descripcion, @RequestParam Tipo tipo) {
			// creando objeto de la clase animales//
			Tareas an = new Tareas();
			
			an = new Tareas(codigo, titulo, descripcion,tipo);

			HashMap<String, String> jsonReturn = new HashMap<>();

			try {
				// guardando el objeto de la clase animales//
				rTarea.save(an);

				// Mensajes deConfirmacion //
				jsonReturn.put("Estado", "OK");
				jsonReturn.put("Mensaje", "Registro guardado");

				// Retornando_Mensaje //
				return jsonReturn;
			} catch (Exception e) {

				// Mensajes_De_Confirmacion //
				jsonReturn.put("Estado", "Error");
				jsonReturn.put("Mensaje", "Registro no guardado");

				// Retornando Mensaje //
				return jsonReturn;

			}
		}
	@PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Tareas getMethodName(@PathVariable Long id) {
		return rTarea.findById(id).get();
	}

	/** Metodo_para_eliminar_registro */
	@DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Boolean delete(@PathVariable Long id) {
		Tareas e = rTarea.findById(id).get();
		rTarea.delete(e);
		return true;
	}

}
