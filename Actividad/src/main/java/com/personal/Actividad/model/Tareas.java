package com.personal.Actividad.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * tareas
 */
@Entity
@Table(name = "tareas")
public class Tareas {
	
	public enum Tipo{
		Personal,Familiar,Trabajo
	}
	@Id
    @Column(nullable = false, length = 30, columnDefinition = "char")
	private Long codigo;
	
	private String titulo;
	
	@Column(nullable = false, length = 30, columnDefinition = "text")
	private String descripcion;

	 @Enumerated(EnumType.STRING)
	    @Column(nullable = false, length = 30, columnDefinition = "ENUM('Personal', 'Familiar', 'Trabajo')")
	    private Tipo tipo;

	public Tareas() {
	}

	public Tareas(Long codigo, String titulo, String descripcion, Tipo tipo) {
		super();
		this.codigo = codigo;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.tipo = tipo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
}
