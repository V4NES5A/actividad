let tarea = {
    id: 0
}

function setIdTarea(id) {
    tarea.codigo = id
}

$(document).ready(inicio);

//FUNCION INICIO
function inicio() {
    cargarDatos();
    $("#btnGuardar").click(guardar);
    $("#btnEliminar").click(function () {
        eliminar(tarea.codigo)
    });
    $("#btnActualizar").click(modificar);
    $("#btnCancelar").click(reset);
}

function reset() {
    $("#titulo").val(null);
    $("#descripcion").val(null);

    $("#titulo2").val(null);
    $("#descripcion2").val(null);
}

//CARGANDO DATOS A TABLA ESPECIALIDADES
function cargarDatos() {
    $.ajax({
        url: "http://localhost:8081/tareas/all",
        method: "Get",
        data: null,
        success: function (response) {
            $("#datos").html("");

            for (let i = 0; i < response.length; i++) {
                $("#datos").append(
                    "<tr>" +
                    "<td><strong>" + response[i].codigo + "</strong></td>" +
                    "<td><strong>" + response[i].titulo + "</strong></td>" +
                    "<td><strong>" + response[i].descripcion + "</strong></td>" +
                    "<td><strong>" + response[i].tipo + "</strong></td>" +
                    "<td>" +
                    "<button onclick='cargarRegistro(" + response[i].codigo +
                    ")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i> <strong>Editar</strong></button>" +
                    "<button onclick='setIdTarea(" + response[i].codigo +
                    ");' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> <strong>Eliminar</strong></button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function (response) {
            alert("Error: " + response);
        }
    });
}

function guardar(response) {
    $.ajax({
        url: "http://localhost:8081/tareas/save",
        method: "Put",
        data: {
            id: null,
            titulo: $("#titulo").val(),
            descripcion: $("#descripcion").val(),
            tipo: $("#tipo").val()
        },
        success: function () {
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function eliminar(id) {
    $.ajax({
        url: "http://localhost:8081/tareas/" + id,
        method: "Delete",
        success: function () {
            cargarDatos();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function cargarRegistro(id) {
    $.ajax({
        url: "http://localhost:8081/tareas/" + id,
        method: "Post",
        success: function (response) {
            $("#id2").val(response.codigo)
            $("#titulo2").val(response.titulo)
            $("#descripcion2").val(response.descripcion)
            $("#tipo2").val(response.tipo)
        },
        error: function (response) {
            alert("Error en la peticion " + response);
        }
    })
}

function modificar() {
    var id = $("#id2").val();
    $.ajax({
        url: "http://localhost:8081/tareas/update/",
        method: "put",
        data: {
            id: id,
            titulo: $("#titulo2").val(),
            descripcion: $("#descripcion2").val(),
            titulo: $("titulo2").val()
        },
        success: function (response) {
            console.log(response);
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response);
            console.log(id);
        }
    });
}